package com.haud.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.MonthDay;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CustomerRetrieverImpl implements CustomerRetriever {

    private final CustomerRepository customerRepository;

    @Override
    public Optional<Customer> byId(final long id) {
        return customerRepository.findById(id).map(customer -> customer);
    }

    @Override
    public List<Customer> byBirthday(final MonthDay birthDay) {
        return customerRepository.findByMonthOfBirthAndDayOfBirth(birthDay.getMonthValue(), birthDay.getDayOfMonth());
    }
}
