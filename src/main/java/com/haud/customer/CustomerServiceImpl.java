package com.haud.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.MonthDay;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
class CustomerServiceImpl implements CustomerService {

    private final CustomerCreator customerCreator;
    private final CustomerRetriever customerRetriever;

    @Override
    public Customer createCustomer(final CreateCustomerData customer) {
        return customerCreator.create(customer);
    }

    @Override
    public Optional<Customer> getCustomer(final long customerId) {
        return customerRetriever.byId(customerId);
    }

    @Override
    public List<Customer> getCustomers(final MonthDay birthday) {
        return customerRetriever.byBirthday(birthday);
    }
}
