package com.haud.customer;

import java.time.MonthDay;
import java.util.List;
import java.util.Optional;

public interface CustomerRetriever {

    Optional<Customer> byId(long id);

    List<Customer> byBirthday(MonthDay birthDay);
}
