package com.haud.customer;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateCustomerDataImpl implements CreateCustomerData {
    private final String name;
    private final String surname;
    private final LocalDate dateOfBirth;
}
