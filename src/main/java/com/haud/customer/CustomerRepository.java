package com.haud.customer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

interface CustomerRepository extends CrudRepository<CustomerEntity, Long> {

    List<Customer> findByMonthOfBirthAndDayOfBirth(int montOfBirth, int dayOfBirth);
}
