package com.haud.customer;

@FunctionalInterface
interface CustomerCreator {
    Customer create(CreateCustomerData customerData);
}
