package com.haud.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
@Transactional
class CustomerCreatorImpl implements CustomerCreator {

    private final CustomerRepository customerRepository;

    @Override
    public Customer create(final CreateCustomerData customerData) {
        return customerRepository.save(new CustomerEntity(customerData.getName(), customerData.getSurname(), customerData.getDateOfBirth()));
    }
}
