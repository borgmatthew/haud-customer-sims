package com.haud.customer;

import java.time.LocalDate;

public interface Customer {

    Long getId();

    String getName();

    String getSurname();

    LocalDate getDateOfBirth();
}
