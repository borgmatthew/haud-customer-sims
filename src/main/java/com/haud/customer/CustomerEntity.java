package com.haud.customer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "customer")
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class CustomerEntity implements Customer {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "yearOfBirth", nullable = false)
    private int yearOfBirth;

    @Column(name = "monthOfBirth", nullable = false)
    private int monthOfBirth;

    @Column(name = "dayOfBirth", nullable = false)
    private int dayOfBirth;

    CustomerEntity(final String name, final String surname, final LocalDate dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = dateOfBirth.getYear();
        this.monthOfBirth = dateOfBirth.getMonthValue();
        this.dayOfBirth = dateOfBirth.getDayOfMonth();
    }

    CustomerEntity(final long id, final String name, final String surname, final LocalDate dateOfBirth) {
        this(name, surname, dateOfBirth);
        this.id = id;
    }

    @Override
    public LocalDate getDateOfBirth() {
        return LocalDate.of(yearOfBirth, monthOfBirth, dayOfBirth);
    }
}
