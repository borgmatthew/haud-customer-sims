package com.haud.customer;

import java.time.MonthDay;
import java.util.List;
import java.util.Optional;

public interface CustomerService {

    Customer createCustomer(final CreateCustomerData customer);

    Optional<Customer> getCustomer(final long customerId);

    List<Customer> getCustomers(final MonthDay birthday);
}
