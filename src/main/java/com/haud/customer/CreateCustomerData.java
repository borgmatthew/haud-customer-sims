package com.haud.customer;

import java.time.LocalDate;

public interface CreateCustomerData {

    String getName();

    String getSurname();

    LocalDate getDateOfBirth();
}
