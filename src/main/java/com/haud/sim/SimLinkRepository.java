package com.haud.sim;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

interface SimLinkRepository extends CrudRepository<SimLinkEntity, Long> {

    Optional<SimLink> findBySimId(long simId);
}
