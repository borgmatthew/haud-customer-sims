package com.haud.sim;

@FunctionalInterface
interface SimCreator {
    Sim create(CreateSimData createSimData);
}
