package com.haud.sim;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Transactional
public class SimRetrieverImpl implements SimRetriever {

    private final SimRepository simRepository;

    @Override
    public List<Sim> getAll() {
        return new ArrayList<>(simRepository.findAll());
    }

    @Override
    public Optional<Sim> byId(final long id) {
        return simRepository.findById(id).map(entity -> entity);
    }

    @Override
    public List<Sim> getFiltered(final SimFilter filter) {
        final List<Specification<SimEntity>> specifications = new ArrayList<>();
        filter.byCustomerId().ifPresent(customerId -> specifications.add(SimSpecifications.byCustomerId(customerId)));
        return new ArrayList<>(specifications.stream().reduce(Specification::and).map(simRepository::findAll).orElseGet(simRepository::findAll));
    }
}
