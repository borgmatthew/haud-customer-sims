package com.haud.sim;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Optional;

@Entity
@Table(name = "sim")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class SimEntity implements Sim {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "number", nullable = false, length = 8)
    private String number;

    @OneToOne
    @JoinColumn(name = "id")
    private SimLinkEntity simLinkEntity;

    SimEntity(final String number) {
        this.number = number;
    }

    @Override
    public Optional<Long> getCustomerId() {
        return Optional.ofNullable(simLinkEntity).map(SimLinkEntity::getCustomerId);
    }
}
