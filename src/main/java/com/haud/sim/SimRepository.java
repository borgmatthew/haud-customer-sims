package com.haud.sim;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

interface SimRepository extends CrudRepository<SimEntity, Long>, JpaSpecificationExecutor<SimEntity> {

    List<SimEntity> findAll();
}
