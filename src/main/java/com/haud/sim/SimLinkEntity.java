package com.haud.sim;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sim_link")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class SimLinkEntity implements SimLink {

    @Id
    @Column(name = "simId", nullable = false)
    private Long simId;

    @Column(name = "customerId", nullable = false)
    private Long customerId;

    @OneToOne(mappedBy = "simLinkEntity", optional = false)
    private SimEntity simEntity;

    SimLinkEntity(final Long simId, final Long customerId) {
        this.simId = simId;
        this.customerId = customerId;
    }
}
