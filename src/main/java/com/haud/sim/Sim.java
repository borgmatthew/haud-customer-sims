package com.haud.sim;

import java.util.Optional;

public interface Sim {

    Long getId();

    String getNumber();

    Optional<Long> getCustomerId();
}
