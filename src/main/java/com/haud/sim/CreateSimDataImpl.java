package com.haud.sim;

import lombok.Data;

@Data
public class CreateSimDataImpl implements CreateSimData {
    private final String number;
}
