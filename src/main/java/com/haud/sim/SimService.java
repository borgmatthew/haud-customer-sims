package com.haud.sim;

import java.util.List;

public interface SimService {

    Sim createSim(CreateSimData createSimData);

    List<Sim> getAllSims();

    Sim linkSimToCustomer(final long simId, final long customerId);

    List<Sim> getFilteredSims(SimFilter simsFilter);
}
