package com.haud.sim;

@FunctionalInterface
public interface CustomerSimLinkCreator {
    SimLink create(final long simId, final long customerId);
}
