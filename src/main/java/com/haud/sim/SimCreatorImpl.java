package com.haud.sim;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
@Transactional
public class SimCreatorImpl implements SimCreator {

    private final SimRepository simRepository;

    @Override
    public Sim create(final CreateSimData createSimData) {
        return simRepository.save(new SimEntity(createSimData.getNumber()));
    }
}
