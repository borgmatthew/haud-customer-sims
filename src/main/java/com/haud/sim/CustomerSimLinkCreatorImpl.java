package com.haud.sim;

import com.haud.customer.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@RequiredArgsConstructor
@Transactional
public class CustomerSimLinkCreatorImpl implements CustomerSimLinkCreator {

    private final SimLinkRepository simLinkRepository;
    private final SimRetriever simRetriever;
    private final CustomerService customerService;

    @Override
    public SimLink create(final long simId, final long customerId) {
        final Optional<Sim> sim = simRetriever.byId(simId);
        if (!sim.isPresent()) {
            throw new InvalidLinkException("Sim not found");
        }

        if (!customerService.getCustomer(customerId).isPresent()) {
            throw new InvalidLinkException("Customer not found");
        }

        return simLinkRepository.save(new SimLinkEntity(simId, customerId));
    }
}