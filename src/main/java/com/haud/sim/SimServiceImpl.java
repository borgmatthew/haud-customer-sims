package com.haud.sim;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SimServiceImpl implements SimService {

    private final SimCreator simCreator;
    private final SimRetriever simRetriever;
    private final CustomerSimLinkCreator customerSimLinkCreator;

    @Override
    public Sim createSim(final CreateSimData createSimData) {
        return simCreator.create(createSimData);
    }

    @Override
    public List<Sim> getAllSims() {
        return simRetriever.getAll();
    }

    @Override
    public Sim linkSimToCustomer(final long simId, final long customerId) {
        customerSimLinkCreator.create(simId, customerId);
        return simRetriever.byId(simId).orElseThrow(() -> new IllegalStateException("Sim not found!"));
    }

    @Override
    public List<Sim> getFilteredSims(final SimFilter simsFilter) {
        return simRetriever.getFiltered(simsFilter);
    }
}
