package com.haud.sim;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class SimSpecifications {

    static Specification<SimEntity> byCustomerId(final long customerId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("simLinkEntity").get("customerId"), customerId);
    }
}
