package com.haud.sim;

import java.util.List;
import java.util.Optional;

public interface SimRetriever {

    List<Sim> getAll();

    Optional<Sim> byId(long id);

    List<Sim> getFiltered(SimFilter filter);
}
