package com.haud.sim;

public class InvalidLinkException extends RuntimeException {
    public InvalidLinkException(final String message) {
        super(message);
    }
}
