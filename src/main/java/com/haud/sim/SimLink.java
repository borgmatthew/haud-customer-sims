package com.haud.sim;

public interface SimLink {

    Long getSimId();

    Long getCustomerId();
}
