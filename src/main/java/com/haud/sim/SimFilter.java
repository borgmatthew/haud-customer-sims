package com.haud.sim;

import java.util.Optional;

public interface SimFilter {
    Optional<Long> byCustomerId();
}
