package com.haud.scheduler.birthday.export;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@EqualsAndHashCode
@Getter
@RequiredArgsConstructor
class BirthdayExportSchedulerUniqueKey {
    private final LocalDate day;
}
