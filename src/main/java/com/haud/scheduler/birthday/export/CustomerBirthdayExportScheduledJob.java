package com.haud.scheduler.birthday.export;

import com.haud.customer.Customer;
import com.haud.customer.CustomerService;
import com.haud.scheduler.service.SchedulerService;
import com.haud.sim.SimService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.MonthDay;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
class CustomerBirthdayExportScheduledJob {


    private final CustomerService customerService;
    private final SchedulerService schedulerService;
    private final SimService simService;
    private final BirthdayFileExporter fileExporter;

    @Scheduled(cron = "0 0 2 * * *")
    @SchedulerLock(name = "CustomerBirthdayExportScheduledJob_export", lockAtLeastFor = "1M", lockAtMostFor = "PT15M")
    public void export() {
        final LocalDate today = LocalDate.now();
        final BirthdayExportSchedulerUniqueKey uniqueKey = new BirthdayExportSchedulerUniqueKey(today);
        final BirthdayExportSchedulerEntry schedulerEntry = new BirthdayExportSchedulerEntry(uniqueKey);

        if (schedulerService.isProcessed(schedulerEntry)) {
            return;
        }

        log.info(String.format("Exporting daily birthday file for date %s", today));

        final List<BirthdayFileEntry> fileEntries = customerService.getCustomers(MonthDay.from(today))
                                                                   .stream()
                                                                   .map(this::process)
                                                                   .collect(Collectors.toList());
        fileExporter.export(fileEntries);
        schedulerService.markAsProcessed(schedulerEntry);
    }

    private BirthdayFileEntry process(final Customer customer) {
        return new BirthdayFileEntry(customer, simService.getFilteredSims(() -> Optional.of(customer.getId())));
    }
}
