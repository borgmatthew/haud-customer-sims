package com.haud.scheduler.birthday.export;

import com.haud.scheduler.service.SchedulerEntry;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BirthdayExportSchedulerEntry implements SchedulerEntry {

    private static final String KEY = "BIRTHDAY_EXPORT";

    private final BirthdayExportSchedulerUniqueKey schedulerUniqueKey;

    @Override
    public String getJobKey() {
        return KEY;
    }

    @Override
    public long getUniqueKey() {
        return schedulerUniqueKey.hashCode();
    }
}
