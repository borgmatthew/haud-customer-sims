package com.haud.scheduler.birthday.export;

import com.haud.customer.Customer;
import com.haud.sim.Sim;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Getter
class BirthdayFileEntry {
    private final Customer customer;
    private final List<Sim> customerSims;
}
