package com.haud.scheduler.birthday.email;

import com.haud.customer.Customer;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;

@EqualsAndHashCode
@Getter
class BirthdayEmailSchedulerUniqueKey {

    private final int ageNextBirthday;
    private final long customerId;

    BirthdayEmailSchedulerUniqueKey(final Customer customer) {
        this.customerId = customer.getId();
        this.ageNextBirthday = LocalDate.now().minusYears(customer.getDateOfBirth().getYear()).getYear() + 1;
    }
}
