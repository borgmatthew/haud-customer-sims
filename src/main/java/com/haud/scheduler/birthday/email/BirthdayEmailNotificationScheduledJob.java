package com.haud.scheduler.birthday.email;

import com.haud.customer.Customer;
import com.haud.customer.CustomerService;
import com.haud.scheduler.service.SchedulerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.MonthDay;

@Component
@Slf4j
@RequiredArgsConstructor
class BirthdayEmailNotificationScheduledJob {


    private final CustomerService customerService;
    private final SchedulerService schedulerService;
    private final BirthdayEmailSender emailSender;

    @Scheduled(cron = "0 0 1 * * *")
    @SchedulerLock(name = "BirthdayEmailNotificationScheduledJob_sendEmails", lockAtLeastFor = "1M", lockAtMostFor = "PT15M")
    public void sendEmails() {
        customerService.getCustomers(MonthDay.from(LocalDate.now().plusDays(7L))).forEach(this::process);
    }

    private void process(final Customer customer) {
        final BirthdayEmailSchedulerUniqueKey uniqueKey = new BirthdayEmailSchedulerUniqueKey(customer);
        final BirthdayEmailSchedulerEntry schedulerEntry = new BirthdayEmailSchedulerEntry(uniqueKey);

        if (schedulerService.isProcessed(schedulerEntry)) {
            return;
        }

        log.info(String.format("Processing customer %s for birthday %d", customer.getId(), uniqueKey.getAgeNextBirthday()));
        emailSender.send(customer);
        schedulerService.markAsProcessed(schedulerEntry);
    }
}
