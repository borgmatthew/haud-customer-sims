package com.haud.scheduler.birthday.email;

import com.haud.scheduler.service.SchedulerEntry;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class BirthdayEmailSchedulerEntry implements SchedulerEntry {

    private static final String KEY = "BIRTHDAY_EMAIL";

    private final BirthdayEmailSchedulerUniqueKey schedulerUniqueKey;

    @Override
    public String getJobKey() {
        return KEY;
    }

    @Override
    public long getUniqueKey() {
        return schedulerUniqueKey.hashCode();
    }
}
