package com.haud.scheduler.birthday.email;

import com.haud.customer.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class BirthdayEmailSender {

    private final JavaMailSender javaMailSender;
    private final String email;

    public BirthdayEmailSender(final JavaMailSender javaMailSender, @Value("${haud.birthday-email-text}") final String email) {
        this.javaMailSender = javaMailSender;
        this.email = email;
    }

    void send(final Customer customer) {
        final SimpleMailMessage message = new SimpleMailMessage();
        // This would need to be replaced by the actual customer's email. A field needs to be added to the customer.
        message.setTo("customer@test.test");
        message.setSubject("Happy Birthday " + customer.getName() + '!');
        message.setText(email);
        message.setFrom("birthday-wishes@test.test");
        javaMailSender.send(message);
    }
}
