package com.haud.scheduler.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class SchedulerServiceImpl implements SchedulerService {

    private final SchedulerRepository schedulerRepository;

    @Override
    public boolean isProcessed(final SchedulerEntry schedulerEntry) {
        return schedulerRepository.findByJobKeyAndUniqueKey(schedulerEntry.getJobKey(), schedulerEntry.getUniqueKey()).isPresent();
    }

    @Override
    public void markAsProcessed(final SchedulerEntry schedulerEntry) {
        schedulerRepository.save(new SchedulerEntity(schedulerEntry.getJobKey(), schedulerEntry.getUniqueKey()));
    }
}
