package com.haud.scheduler.service;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SchedulerRepository extends CrudRepository<SchedulerEntity, Long> {

    Optional<SchedulerEntity> findByJobKeyAndUniqueKey(final String jobKey, final long uniqueKey);
}
