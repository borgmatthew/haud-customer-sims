package com.haud.scheduler.service;

public interface SchedulerService {

    boolean isProcessed(SchedulerEntry schedulerEntry);

    void markAsProcessed(SchedulerEntry schedulerEntry);
}
