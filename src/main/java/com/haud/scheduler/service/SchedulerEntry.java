package com.haud.scheduler.service;

public interface SchedulerEntry {

    String getJobKey();

    long getUniqueKey();
}
