package com.haud.scheduler.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "scheduler", uniqueConstraints = { @UniqueConstraint( columnNames = { "jobKey", "uniqueKey" } ) })
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class SchedulerEntity {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "jobKey", nullable = false)
    private String jobKey;

    @Column(name = "uniqueKey", nullable = false)
    private long uniqueKey;

    public SchedulerEntity(final String jobKey, final long uniqueKey) {
        this.jobKey = jobKey;
        this.uniqueKey = uniqueKey;
    }
}
