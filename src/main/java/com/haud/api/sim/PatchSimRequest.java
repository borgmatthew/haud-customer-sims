package com.haud.api.sim;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
class PatchSimRequest {

    @JsonProperty("customerId")
    @NotNull(message = "Invalid customer ID")
    private Long customerId;
}
