package com.haud.api.sim;

import com.haud.sim.SimFilter;
import lombok.Data;

import java.util.Optional;

@Data
class GetSimsFilter implements SimFilter {
    private Long customerId;

    @Override
    public Optional<Long> byCustomerId() {
        return Optional.ofNullable(customerId);
    }

    boolean isEmtpy() {
        return !byCustomerId().isPresent();
    }
}
