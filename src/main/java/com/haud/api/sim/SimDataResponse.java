package com.haud.api.sim;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
class SimDataResponse {
    private final Long id;
    private final String number;
    private final Long customerId;
}
