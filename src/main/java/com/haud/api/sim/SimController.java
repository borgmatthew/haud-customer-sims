package com.haud.api.sim;

import com.haud.sim.CreateSimDataImpl;
import com.haud.sim.Sim;
import com.haud.sim.SimService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/sims")
@RequiredArgsConstructor
public class SimController {

    private final SimService simService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SimDataResponse createSim(@Valid @RequestBody @NotNull final CreateSimRequest createSimRequest) {
        final Sim sim = simService.createSim(new CreateSimDataImpl(createSimRequest.getNumber()));
        return mapSimToResponse(sim);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SimDataResponse> getAllSims(final GetSimsFilter getSimsFilter) {
        if (getSimsFilter.isEmtpy()) {
            return simService.getAllSims().stream().map(this::mapSimToResponse).collect(Collectors.toList());
        }

        return simService.getFilteredSims(getSimsFilter).stream().map(this::mapSimToResponse).collect(Collectors.toList());
    }

    private SimDataResponse mapSimToResponse(final Sim sim) {
        final SimDataResponse.SimDataResponseBuilder responseBuilder = SimDataResponse.builder().id(sim.getId()).number(sim.getNumber());
        sim.getCustomerId().ifPresent(responseBuilder::customerId);
        return responseBuilder.build();
    }

    @PatchMapping(path = "/{sim-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SimDataResponse patchSim(@NotNull @PathVariable("sim-id") final Long simId,
                                    @Valid @RequestBody @NotNull final PatchSimRequest patchRequest) {
        return mapSimToResponse(simService.linkSimToCustomer(simId, patchRequest.getCustomerId()));
    }
}
