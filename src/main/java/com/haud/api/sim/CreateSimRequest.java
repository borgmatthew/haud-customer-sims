package com.haud.api.sim;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
class CreateSimRequest {

    @JsonProperty("number")
    @Pattern(regexp = "[0-9]{8}", message = "Invalid sim number format")
    private String number;
}
