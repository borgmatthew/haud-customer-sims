package com.haud.api.customer;

import com.haud.customer.CreateCustomerDataImpl;
import com.haud.customer.Customer;
import com.haud.customer.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CustomerResponse createCustomer(@Valid @RequestBody @NotNull final CustomerRequest customerRequest) {
        final Customer customer = customerService.createCustomer(
                new CreateCustomerDataImpl(customerRequest.getName(), customerRequest.getSurname(), customerRequest.getDateOfBirth()));
        return CustomerResponse.builder()
                               .id(customer.getId())
                               .name(customer.getName())
                               .surname(customer.getSurname())
                               .dateOfBirth(customer.getDateOfBirth())
                               .build();
    }
}
