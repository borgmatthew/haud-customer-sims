package com.haud.api.customer;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Builder
@Data
class CustomerResponse {
    private final Long id;
    private final String name;
    private final String surname;
    private final LocalDate dateOfBirth;
}
