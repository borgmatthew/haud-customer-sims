package com.haud.api.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Data
@NoArgsConstructor
class CustomerRequest {

    @JsonProperty("name")
    @Pattern(regexp = "[a-zA-Z]{1,20}", message = "Invalid name format")
    @NotNull(message = "name is required")
    private String name;

    @JsonProperty("surname")
    @Pattern(regexp = "[a-zA-Z]{1,20}", message = "Invalid surname format")
    @NotNull(message = "surname is required")
    private String surname;

    @JsonProperty("dateOfBirth")
    @JsonFormat(pattern="dd-MM-yyyy")
    @NotNull(message = "dateOfBirth is required")
    private LocalDate dateOfBirth;
}
