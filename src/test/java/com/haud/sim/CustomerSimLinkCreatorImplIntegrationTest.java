package com.haud.sim;

import com.haud.Application;
import com.haud.customer.Customer;
import com.haud.customer.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = Application.class)
@Transactional
class CustomerSimLinkCreatorImplIntegrationTest {

    private static final long SIM_ID = 123L;
    private static final long CUSTOMER_ID = 456L;

    @Autowired
    private CustomerSimLinkCreator customerSimLinkCreator;

    @Autowired
    private SimLinkRepository simLinkRepository;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private SimRetriever simRetriever;

    @Test
    void createSimLink_linkPersistedCorrectlyToDatabase() {
        when(customerService.getCustomer(CUSTOMER_ID)).thenReturn(Optional.of(mock(Customer.class)));
        when(simRetriever.byId(SIM_ID)).thenReturn(Optional.of(mock(Sim.class)));

        customerSimLinkCreator.create(SIM_ID, CUSTOMER_ID);

        final Optional<SimLinkEntity> persistedLink = simLinkRepository.findById(SIM_ID);
        assertThat(persistedLink).isPresent().contains(new SimLinkEntity(SIM_ID, CUSTOMER_ID));
    }
}