package com.haud.sim;

import com.haud.customer.CustomerService;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CustomerSimLinkCreatorImplTest {

    private static final long SIM_ID = 123L;
    private static final long CUSTOMER_ID = 456L;

    private final SimLinkRepository simLinkRepository = mock(SimLinkRepository.class);
    private final SimRetriever simRetriever = mock(SimRetriever.class);
    private final CustomerService customerService = mock(CustomerService.class);
    private final CustomerSimLinkCreatorImpl customerSimLinkCreator = new CustomerSimLinkCreatorImpl(simLinkRepository, simRetriever,
            customerService);

    @Test
    void create_simNotFound_throwsException() {
        when(simRetriever.byId(SIM_ID)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> customerSimLinkCreator.create(SIM_ID, CUSTOMER_ID)).isInstanceOf(InvalidLinkException.class)
                                                                                    .hasMessage("Sim not found");
    }

    @Test
    void create_customerNotFound_throwsException() {
        when(simRetriever.byId(SIM_ID)).thenReturn(Optional.of(mock(Sim.class)));
        when(customerService.getCustomer(CUSTOMER_ID)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> customerSimLinkCreator.create(SIM_ID, CUSTOMER_ID)).isInstanceOf(InvalidLinkException.class)
                                                                                    .hasMessage("Customer not found");
    }
}