package com.haud.sim;

import com.haud.Application;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = Application.class)
@Transactional
class SimCreatorImplIntegrationTest {

    private static final String NUMBER = "12345678";

    @Autowired
    private SimCreator simCreator;

    @Autowired
    private SimRepository simRepository;

    @Test
    void createSim_simPersistedCorrectlyToDatabase() {
        final Sim createdSim = simCreator.create(new CreateSimDataImpl(NUMBER));

        final Optional<SimEntity> persistedSim = simRepository.findById(createdSim.getId());

        assertThat(persistedSim).isPresent().contains(new SimEntity(createdSim.getId(), NUMBER, null));
    }

}