package com.haud.customer;

import com.haud.Application;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = Application.class)
@Transactional
class CustomerCreatorImplIntegrationTest {

    private static final String NAME = "matthew";
    private static final String SURNAME = "borg";
    private static final LocalDate DATE_OF_BIRTH = LocalDate.of(1994, 8, 11);

    @Autowired
    private CustomerCreatorImpl customerCreator;

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void createCustomer_customerPersistedCorrectlyToDatabase() {
        final Customer createdCustomer = customerCreator.create(new CreateCustomerDataImpl(NAME, SURNAME, DATE_OF_BIRTH));

        final Optional<CustomerEntity> persistedCustomer = customerRepository.findById(createdCustomer.getId());

        assertThat(persistedCustomer).isPresent().contains(new CustomerEntity(createdCustomer.getId(), NAME, SURNAME, DATE_OF_BIRTH));
    }
}