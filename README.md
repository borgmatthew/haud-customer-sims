# Haud customer sims

All of the required functionality is implemented and complete, except for the daily export where I have implemented a scheduled job which retrieves
 all the required data, however it does not export the data to a file. Also, the email notification does not have any place-holders.
 
I've decided to leave this part out as there are multiple possible ways how an operator would want the file to be exported. Having all the data 
available would make the file exportation a simple task. I would have added a service which handles files, such as `Minio`, and write the file to 
disk using this service.

The major decision I had to make was how to handle the linking between sim cards and customers, which I handled by implementing a PATCH request on 
the sim. I had the option to do the linking the other way round, by doing a PATCH on the customer to update the list of related sims. However, that 
would have been more complicated to understand and implement, since you would be updating a collection. Doing a PATCH on the sim is easier to 
understand and work with.

I've focused mainly on the design of the system and tried to make it as easy as possible to change.

## Building the project

To build the project, simply run `mvn clean install` at the root directory

## Executing the project

The following steps are required:

* Run the image in the docker-compose file: `docker-compose -f docker-compose-haud up`. This runs a mailhog email testing tool
* Run the spring-boot app: `mvn spring-boot:run`

## API

The API is documented in the provided `swagger.yaml` file (in resources folder)

## Improvements

If I had more time, I would have added the following:
* Validation on sim card numbers
* An email field to the customer (and its related validation)
* Template support for the emails
* Proper error handling for the controllers using `@ControllerAdvice` to return correct Http status codes
* Implement the file creation for the daily export
* Add paging to the APIs